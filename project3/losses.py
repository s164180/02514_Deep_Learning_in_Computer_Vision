import torch
import torch.nn as nn
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class BCELoss(nn.Module):

    def __init__(self):
        super(BCELoss, self).__init__()
        self.loss = nn.BCEWithLogitsLoss()

    def __call__(self, prediction, target_is_real):
        # Reshape target label (0 is fake, 1 is real)
        target_tensor = torch.tensor(1.0, device=device) if target_is_real else torch.tensor(0.0, device=device)
        target_tensor = target_tensor.expand_as(prediction)
        loss = self.loss(prediction, target_tensor)
        return loss


class LSGANLoss(nn.Module):

    def __init__(self):
        super(LSGANLoss, self).__init__()
        self.loss = nn.MSELoss()

    def __call__(self, prediction, target_is_real):
        # Reshape target label (0 is fake, 1 is real)
        target_tensor = torch.tensor(1.0, device=device) if target_is_real else torch.tensor(0.0, device=device)
        target_tensor = target_tensor.expand_as(prediction)
        loss = self.loss(prediction, target_tensor)
        return loss
