import torch.nn as nn
import torch.nn.functional as F
norm_layer = nn.InstanceNorm2d
import torch
import torchvision.transforms as transforms

# c7s1-k (7x7 Conv2d with stride 1 and k filters) <br>
# dk (3x3 Convolution-InstanceNorm-ReLU with k filters) <br>
# Rk (residual block that contains two 3 × 3 convolutional layers with k filters on both layer) <br>
# uk (3×3 fractional-strided-Convolution-InstanceNorm-ReLU layer with k filters and stride 1/2) <br>
# Ck (4×4 Convolution-InstanceNorm-LeakyReLU layer with k filters and stride 2)


class CycleGAN(nn.Module):
    def __init__(self, gan_loss_fn, blocks=6):
        super(CycleGAN, self).__init__()

        self.gen_transforms = transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        self.genA2B = Generator(blocks=blocks)
        self.genB2A = Generator(blocks=blocks)
        self.discrimA = PatchGAN()
        self.discrimB = PatchGAN()
        self.discrims = [self.discrimA, self.discrimB]

        self.cycle_loss = torch.nn.L1Loss()
        self.identity_loss = torch.nn.L1Loss()
        self.gan_loss = gan_loss_fn

    def generate(self, x, input_is_A):
        return self.genA2B(x) if input_is_A else self.genB2A(x)

    def discriminate(self, x, input_is_A):
        return self.discrimA(x) if input_is_A else self.discrimB(x)

    def forward_basic(self, x, input_is_A):
        for b in range(x.shape[0]):
            x[b] = self.gen_transforms(x[b])
        gen = self.generate(x, input_is_A=input_is_A)
        rec = self.generate(gen, input_is_A=~input_is_A)
        return gen, rec

    def gen_loss(self, x_A, x_B):
        lambda_cycle = 10
        lambda_identity = 5

        self.gen_B, self.rec_A = self.forward_basic(x_A, input_is_A=True)
        self.gen_A, self.rec_B = self.forward_basic(x_B, input_is_A=False)

        # GANloss(D_B(b_fake), 1) [target is real is switched]
        loss = self.gan_loss(self.discriminate(self.gen_B, input_is_A=False), target_is_real=True)

        # ImLoss(a_rec, a_real)
        loss += lambda_cycle * self.cycle_loss(self.rec_A, x_A)

        # ImLoss(B2A(a_real), a_real)
        loss += lambda_identity * self.identity_loss(self.generate(x_A, input_is_A=False),
                                                     # Even though x_A we use input_is_B to get B2A gen
                                                     x_A)

        # GANloss(D_A(a_fake), 1) [target is real is switched]
        loss += self.gan_loss(self.discriminate(self.gen_A, input_is_A=True), target_is_real=True)

        # ImLoss(b_rec, b_real)
        loss += lambda_cycle * self.cycle_loss(self.rec_B, x_B)

        # ImLoss(A2B(b_real), b_real)
        loss += lambda_identity * self.identity_loss(self.generate(x_B, input_is_A=True),
                                                     # Even though x_B we use input_is_A to get A2B gen
                                                     x_B)

        return loss

    def discrim_A_loss(self, x_A, gen_A_buffer=None):
        # GANloss(D_A(a_real), 1)
        loss = self.gan_loss(self.discriminate(x_A, input_is_A=True), target_is_real=True)

        if gen_A_buffer is not None:
            # Keep batch_size - 1 batches of gen_A
            gen_A = self.gen_A.detach()[:(self.gen_A.shape[0] - 1)]
            # Add buffer to x_A
            gen_A = torch.cat((gen_A, gen_A_buffer), 0)
        else:
            gen_A = x_A

        # GANloss(D_A(a_fake), 0)
        loss += self.gan_loss(self.discriminate(gen_A, input_is_A=True), target_is_real=False)
        return 0.5 * loss

    def discrim_B_loss(self, x_B, gen_B_buffer=None):
        # GANloss(D_B(b_real), 1)
        loss = self.gan_loss(self.discriminate(x_B, input_is_A=False), target_is_real=True)

        if gen_B_buffer is not None:
            # Keep batch_size - 1 batches of gen_A
            gen_B = self.gen_B.detach()[:(self.gen_B.shape[0] - 1)]
            # Add buffer to x_A
            gen_B = torch.cat((gen_B, gen_B_buffer), 0)
        else:
            gen_B = x_B

        # GANloss(D_B(b_fake), 0)
        loss += self.gan_loss(self.discriminate(gen_B, input_is_A=False), target_is_real=False)
        return 0.5 * loss

    def grad_discrim(self, on):
        for D in self.discrims:
            for param in D.parameters():
                param.requires_grad = on


class ResBlock(nn.Module):
    def __init__(self, f):
        super(ResBlock, self).__init__()
        self.conv = nn.Sequential(nn.Conv2d(f, f, 3, 1, 1), norm_layer(f), nn.ReLU(),
                                  nn.Conv2d(f, f, 3, 1, 1))
        self.norm = norm_layer(f)

    def forward(self, x):
        return F.relu(self.norm(self.conv(x)+x))


class Generator(nn.Module):
    def __init__(self, f=64, blocks=6):
        super(Generator, self).__init__()
        layers = [nn.ReflectionPad2d(3),
                  nn.Conv2d(3, f, 7, 1, 0), norm_layer(f), nn.ReLU(True),
                  nn.Conv2d(f, 2 * f, 3, 2, 1), norm_layer(2 * f), nn.ReLU(True),
                  nn.Conv2d(2 * f, 4 * f, 3, 2, 1), norm_layer(4 * f), nn.ReLU(True)]
        for i in range(int(blocks)):
            layers.append(ResBlock(4 * f))
        layers.extend([
            nn.ConvTranspose2d(4 * f, 4 * 2 * f, 3, 1, 1), nn.PixelShuffle(2), norm_layer(2 * f), nn.ReLU(True),
            nn.ConvTranspose2d(2 * f, 4 * f, 3, 1, 1), nn.PixelShuffle(2), norm_layer(f), nn.ReLU(True),
            nn.ReflectionPad2d(3), nn.Conv2d(f, 3, 7, 1, 0),
            nn.Tanh()])
        self.conv = nn.Sequential(*layers)

    def forward(self, x):
        return self.conv(x)


# Architecture: C64, C128, C256, C512
class PatchGAN(nn.Module):
    def __init__(self):
        super(PatchGAN, self).__init__()

        self.hidden_layers = [nn.Conv2d(3, 64, 4, stride=2, padding=1),
                              nn.LeakyReLU(negative_slope=0.2, inplace=True)
                              ]

        # C64, C128, C256
        C_filters = [128, 256, 512]
        strides = [2, 2, 1]
        in_dim = 64
        for stride, filters in zip(strides, C_filters):
            self.hidden_layers.extend([nn.Conv2d(in_dim, filters, 4, stride=stride, padding=1),
                                       nn.InstanceNorm2d(filters),
                                       nn.LeakyReLU(negative_slope=0.2, inplace=True)
                                       ])
            in_dim = filters
        self.hidden_layers.append(nn.Conv2d(512, 1, kernel_size=(4, 4), stride=(1, 1), padding=1))
        self.hidden_layers = nn.Sequential(*self.hidden_layers)

    def forward(self, x):
        return self.hidden_layers(x)
