import torch
import torchvision.transforms as transforms
import numpy.random as npr
from PIL import Image
import os


class ZebraTest(torch.utils.data.Dataset):
    def __init__(self, transform=transforms.ToTensor()):
        'Initialzation'
        self.zebra = None
        self.transform = transform
        self.root = 'horse2zebra2/'
        self.root += 'test/'
        # Load path to images
        for root, dirs, files in os.walk(self.root):
            if root.endswith('B'):
                self.zebra = files
        self.num_zebra = len(self.zebra)

    def convert(self, im_path):
        'Converts image path to tensor of image'
        X = Image.open(im_path).convert('RGB')
        X = self.transform(X)
        return X

    def __len__(self):
        'Returns the total number of samples'

        # There are more Zebras in both the test and training data so we use zebras as our reference point
        return self.num_zebra

    def __getitem__(self, idx):
        'Generates one sample of data'
        # There are more Zebras in both the test and training data so we use zebras as our reference point
        # Pick Zebra and then sample a random horse from our dataset to accompany it

        zebra_path = self.root + 'B/' + self.zebra[idx]

        zebra = self.convert(zebra_path)

        return zebra, zebra_path

    def __repr__(self):
        return f'Horse2zebra({self.train})'


class HorseTest(torch.utils.data.Dataset):
    def __init__(self, transform=transforms.ToTensor()):
        'Initialzation'
        self.horse = None
        self.transform = transform
        self.root = 'horse2zebra2/'
        self.root += 'test/'
        # Load path to images
        for root, dirs, files in os.walk(self.root):
            if root.endswith('A'):
                self.horse = files
        self.num_horse = len(self.horse)

    def convert(self, im_path):
        'Converts image path to tensor of image'
        X = Image.open(im_path).convert('RGB')
        X = self.transform(X)
        return X

    def __len__(self):
        'Returns the total number of samples'

        # There are more Zebras in both the test and training data so we use zebras as our reference point
        return self.num_horse

    def __getitem__(self, idx):
        'Generates one sample of data'
        # There are more Zebras in both the test and training data so we use zebras as our reference point
        # Pick Zebra and then sample a random horse from our dataset to accompany it

        horse_path = self.root + 'A/' + self.horse[idx]

        horse = self.convert(horse_path)

        return horse, horse_path

    def __repr__(self):
        return f'Horse2zebra({self.train})'


class Horse2Zebra(torch.utils.data.Dataset):
    def __init__(self, train, transform=transforms.ToTensor()):
        'Initialzation'
        self.horse = None
        self.zebra = None
        self.train = train
        self.transform = transform
        self.root = 'horse2zebra2/'

        if train:
            self.root += 'train/'
        else:
            self.root += 'test/'
        print(self.root)
        # Load path to images
        for root, dirs, files in os.walk(self.root):
            if root.endswith('B'):  # horse class
                self.zebra = files
            elif root.endswith('A'):
                self.horse = files
            else:
                continue

        self.num_horse = len(self.horse)
        self.num_zebra = len(self.zebra)

    def convert(self, im_path):
        'Converts image path to tensor of image'
        X = Image.open(im_path).convert('RGB')
        X = self.transform(X)
        return X

    def __len__(self):
        'Returns the total number of samples'

        # There are more Zebras in both the test and training data so we use zebras as our reference point
        return self.num_zebra

    def __getitem__(self, idx):
        'Generates one sample of data'
        # There are more Zebras in both the test and training data so we use zebras as our reference point
        # Pick Zebra and then sample a random horse from our dataset to accompany it - also great data augmentation

        zebra_path = self.root + 'B/' + self.zebra[idx]
        horse_path = self.root + 'A/' + self.horse[npr.randint(len(self.horse))]

        zebra = self.convert(zebra_path)
        horse = self.convert(horse_path)

        return zebra, horse

    def __repr__(self):
        return f'Horse2zebra({self.train})'


class Horse2ZebraWithPath(torch.utils.data.Dataset):
    def __init__(self, train, transform=transforms.ToTensor()):
        'Initialzation'
        self.horse = None
        self.zebra = None
        self.train = train
        self.transform = transform
        self.root = 'horse2zebra2/'

        if train:
            self.root += 'train/'
        else:
            self.root += 'test/'
        print(self.root)
        # Load path to images
        for root, dirs, files in os.walk(self.root):
            if root.endswith('B'):  # horse class
                self.zebra = files
            elif root.endswith('A'):
                self.horse = files
            else:
                continue

        self.num_horse = len(self.horse)
        self.num_zebra = len(self.zebra)

    def convert(self, im_path):
        'Converts image path to tensor of image'
        X = Image.open(im_path).convert('RGB')
        X = self.transform(X)
        return X

    def __len__(self):
        'Returns the total number of samples'

        # There are more Zebras in both the test and training data so we use zebras as our reference point
        return self.num_zebra

    def __getitem__(self, idx):
        'Generates one sample of data'
        # There are more Zebras in both the test and training data so we use zebras as our reference point
        # Pick Zebra and then sample a random horse from our dataset to accompany it

        zebra_path = self.root + 'B/' + self.zebra[idx]
        horse_path = self.root + 'A/' + self.horse[npr.randint(len(self.horse))]

        zebra = self.convert(zebra_path)
        horse = self.convert(horse_path)

        return zebra, zebra_path, horse, horse_path

    def __repr__(self):
        return f'Horse2zebra({self.train})'