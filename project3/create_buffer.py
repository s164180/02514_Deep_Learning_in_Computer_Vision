import torch
import torch.nn as nn
import os
import numpy.random as npr
from random import randint, sample
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
from PIL import Image
from torch.utils.data import DataLoader
from pathlib import Path
import torchvision.transforms as transforms

from models import Generator, PatchGAN
from data import Horse2Zebra
from losses import BCELoss, LSGANLoss

import numpy as np


class CycleGAN(nn.Module):
    def __init__(self, gan_loss_fn, blocks=6):
        super(CycleGAN, self).__init__()

        self.gen_transforms = transforms.Normalize((0.5, 0.5, 0.5), (0.5, 0.5, 0.5))
        self.genA2B = Generator(blocks=blocks)
        self.genB2A = Generator(blocks=blocks)
        self.discrimA = PatchGAN()
        self.discrimB = PatchGAN()
        self.discrims = [self.discrimA, self.discrimB]

        self.cycle_loss = torch.nn.L1Loss()
        self.identity_loss = torch.nn.L1Loss()
        self.gan_loss = gan_loss_fn

    def generate(self, x, input_is_A):
        return self.genA2B(x) if input_is_A else self.genB2A(x)

    def discriminate(self, x, input_is_A):
        return self.discrimA(x) if input_is_A else self.discrimB(x)

    def forward_basic(self, x, input_is_A):
        for b in range(x.shape[0]):
            x[b] = self.gen_transforms(x[b])
        gen = self.generate(x, input_is_A=input_is_A)
        rec = self.generate(gen, input_is_A=~input_is_A)
        return gen, rec

    def gen_loss(self, x_A, x_B):
        lambda_cycle = 10
        lambda_identity = 5

        self.gen_B, self.rec_A = self.forward_basic(x_A, input_is_A=True)
        self.gen_A, self.rec_B = self.forward_basic(x_B, input_is_A=False)

        # GANloss(D_B(b_fake), 1) [target is real is switched]
        loss = self.gan_loss(self.discriminate(self.gen_B, input_is_A=False), target_is_real=True)

        # ImLoss(a_rec, a_real)
        loss += lambda_cycle * self.cycle_loss(self.rec_A, x_A)

        # ImLoss(B2A(a_real), a_real)
        loss += lambda_identity * self.identity_loss(self.generate(x_A, input_is_A=False),
                                                     # Even though x_A we use input_is_B to get B2A gen
                                                     x_A)

        # GANloss(D_A(a_fake), 1) [target is real is switched]
        loss += self.gan_loss(self.discriminate(self.gen_A, input_is_A=True), target_is_real=True)

        # ImLoss(b_rec, b_real)
        loss += lambda_cycle * self.cycle_loss(self.rec_B, x_B)

        # ImLoss(A2B(b_real), b_real)
        loss += lambda_identity * self.identity_loss(self.generate(x_B, input_is_A=True),
                                                     # Even though x_B we use input_is_A to get A2B gen
                                                     x_B)

        return loss

    def discrim_A_loss(self, x_A, gen_A_buffer=None):
        # GANloss(D_A(a_real), 1)
        loss = self.gan_loss(self.discriminate(x_A, input_is_A=True), target_is_real=True)

        if gen_A_buffer is not None:
            # Keep batch_size - 1 batches of gen_A
            gen_A = self.gen_A.detach()[:(self.gen_A.shape[0] - 1)]
            # Add buffer to x_A
            gen_A = torch.cat((gen_A, gen_A_buffer), 0)
        else:
            gen_A = x_A

        # GANloss(D_A(a_fake), 0)
        loss += self.gan_loss(self.discriminate(gen_A, input_is_A=True), target_is_real=False)
        return 0.5 * loss

    def discrim_B_loss(self, x_B, gen_B_buffer=None):
        # GANloss(D_B(b_real), 1)
        loss = self.gan_loss(self.discriminate(x_B, input_is_A=False), target_is_real=True)

        if gen_B_buffer is not None:
            # Keep batch_size - 1 batches of gen_A
            gen_B = self.gen_B.detach()[:(self.gen_B.shape[0] - 1)]
            # Add buffer to x_A
            gen_B = torch.cat((gen_B, gen_B_buffer), 0)
        else:
            gen_B = x_B

        # GANloss(D_B(b_fake), 0)
        loss += self.gan_loss(self.discriminate(gen_B, input_is_A=False), target_is_real=False)
        return 0.5 * loss

    def grad_discrim(self, on):
        for D in self.discrims:
            for param in D.parameters():
                param.requires_grad = on


# Saves the current batch buffer
def save_batch_buffer(epoch, batch_num, remove=False):
    for i in range(net.gen_B.shape[0]):
        if remove:
            # Delete a random buffer image to keep buffer size constant
            _, _, files = next(os.walk(f"Image Buffer/{blocks}blocks/A/"))
            file_name_sample = sample(files, 1)[0]
            os.remove(f"Image Buffer/{blocks}blocks/A/" + file_name_sample)
            _, _, files = next(os.walk(f"Image Buffer/{blocks}blocks/B/"))
            file_name_sample = sample(files, 1)[0]
            os.remove(f"Image Buffer/{blocks}blocks/B/" + file_name_sample)
        torch.save(net.gen_A[i], f"Image Buffer/{blocks}blocks/A/" + "Epoch" + str(epoch) + "_" + str(batch_num) + "_" + str(i) + '.pt')
        torch.save(net.gen_B[i], f"Image Buffer/{blocks}blocks/B/" + "Epoch" + str(epoch) + "_" + str(batch_num) + "_" + str(i) + '.pt')


# Loads a random image from the buffer
def load_random_buffer(folder_letter):
    _, _, files = next(os.walk(f"Image Buffer/{blocks}blocks/{folder_letter}/"))
    file_name_sample = sample(files, 1)[0]
    return torch.load(f"Image Buffer/{blocks}blocks/{folder_letter}/" + file_name_sample).unsqueeze(0)


blocks = 9
gan_loss_fn = LSGANLoss()
net = CycleGAN(gan_loss_fn, blocks=blocks)
net.to(device)
net.load_state_dict(torch.load(f"Model Checkpoints/{blocks}blocks/CycleGAN.pth"))
# Separate optimizers for generative and discriminant parts
optim_gen = torch.optim.Adam(list(net.genA2B.parameters()) + list(net.genB2A.parameters()),
                                       lr=0.0002, betas=(0.5, 0.999))
optim_discrim = torch.optim.Adam(list(net.discrimA.parameters()) + list(net.discrimB.parameters()),
                                       lr=0.0002, betas=(0.5, 0.999))
batch_size = 3
data_train = Horse2Zebra(train=True)
train_loader = DataLoader(data_train, batch_size=batch_size, shuffle=True, num_workers=12)
data_test = Horse2Zebra(train=False)
test_loader = DataLoader(data_test, batch_size=batch_size, shuffle=True, num_workers=12)

gen_lst = []
dA_lst = []
dB_lst = []
start_epoch = 2
end_epoch = 2

# Buffer size
B = 50
batch_to_save_inds = sample(range(len(train_loader)), int(B/batch_size))

for epoch in range(start_epoch, end_epoch + 1):
    avg_gen_loss = 0.0
    avg_dA_loss = 0.0
    avg_dB_loss = 0.0

    print(f"Epoch {epoch + 1}/{end_epoch + 1}", end=" ")
    for b, (x_A, x_B) in enumerate(train_loader):
        x_A = x_A.to(device)
        x_B = x_B.to(device)

        # gen_A_buffer = load_random_buffer('A')
        # gen_B_buffer = load_random_buffer('B')

        net.grad_discrim(on=False)
        optim_gen.zero_grad()
        # When gen_loss is called the model forwards all required variables
        gen_loss = net.gen_loss(x_A, x_B)
        gen_loss.backward()
        optim_gen.step()

        net.grad_discrim(on=True)
        optim_discrim.zero_grad()
        dA_loss = net.discrim_A_loss(x_A)  # , gen_A_buffer)
        dA_loss.backward()

        dB_loss = net.discrim_B_loss(x_B)  # , gen_B_buffer)
        dB_loss.backward()
        optim_discrim.step()

        avg_gen_loss += gen_loss.cpu().detach().numpy() / len(train_loader)
        avg_dA_loss += dA_loss.cpu().detach().numpy() / len(train_loader)
        avg_dB_loss += dB_loss.cpu().detach().numpy() / len(train_loader)

        # If batch is sampled, save the generated images.
        if b in batch_to_save_inds:
            save_batch_buffer(epoch, b)

    torch.save(net.state_dict(), f"Model Checkpoints/{blocks}blocks/CycleGAN.pth")
    torch.save(net.genA2B.state_dict(), f"Model Checkpoints/{blocks}blocks/G_A2B.pth")
    torch.save(net.genB2A.state_dict(), f"Model Checkpoints/{blocks}blocks/G_B2A.pth")
    torch.save(net.discrimA.state_dict(), f"Model Checkpoints/{blocks}blocks/D_A.pth")
    torch.save(net.discrimB.state_dict(), f"Model Checkpoints/{blocks}blocks/D_B.pth")

    gen_lst.append(avg_gen_loss)
    dA_lst.append(avg_dA_loss)
    dB_lst.append(avg_dB_loss)
    print("generative loss", avg_gen_loss, end=" ")
    print("discriminant loss A:", avg_dA_loss, end=" ")
    print("B:", avg_dB_loss)

# Append losses and save
gen_loss_epochs = list(np.loadtxt(f"Losses/{blocks}blocks/gen_loss"))
gen_loss_epochs.append(avg_gen_loss)
np.savetxt(f"Losses/{blocks}blocks/gen_loss", np.array(gen_loss_epochs))
d_A_epochs = list(np.loadtxt(f"Losses/{blocks}blocks/d_A_loss"))
d_A_epochs.append(avg_dA_loss)
np.savetxt(f"Losses/{blocks}blocks/d_A_loss", np.array(d_A_epochs))
d_B_epochs = list(np.loadtxt(f"Losses/{blocks}blocks/d_B_loss"))
d_B_epochs.append(avg_dB_loss)
np.savetxt(f"Losses/{blocks}blocks/d_B_loss", np.array(d_B_epochs))
