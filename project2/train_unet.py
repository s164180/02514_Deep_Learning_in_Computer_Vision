from torchvision import transforms
from torch.utils.data import DataLoader
import torch
import copy
from PIL import Image
import matplotlib.pyplot as plt
import torch.nn.functional as F
import torch.nn as nn
from pathlib import Path
import numpy as np
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class LIDC_IDRI(torch.utils.data.Dataset):
    def __init__(self, transform, mode, legions, data_path='LIDC_crops/LIDC_DLCV_version'):
        'Initialization'
        self.transform = transform
        data_path += "/" + mode
        print(data_path)
        self.image_paths = [data_path + "/images/" + path.name for path in Path(data_path + "/images").rglob('*')]

        all_target_paths = [data_path + "/lesions/" + path.name for path in Path(data_path + "/lesions").rglob('*')]

        self.target_paths = []
        for legion in legions:
            legion_path = [path for path in all_target_paths if path.endswith(f"l{legion}.png")]
            self.target_paths.append(legion_path)
        im = Image.open(self.image_paths[0])
        X = self.transform(im)
        self.dim = X.shape[1]

    def __len__(self):
        return len(self.image_paths)

    def get_input_im(self, idx):
        return Image.open(self.image_paths[idx])

    def get_legion_im(self, idx):
        if len(self.target_paths) == 1:
            return Image.open(self.target_paths[0][idx])
        else:
            return None

    def get_x_tensor(self, idx):
        input_im = self.get_input_im(idx)
        return self.transform(input_im)

    def get_y_tensor(self, idx):
        if len(self.target_paths) == 1:
            target_im = self.get_legion_im(idx)
            return self.transform(target_im)
        else:
            return None

    def __getitem__(self, idx):
        X = self.get_x_tensor(idx)
        y = self.get_y_tensor(idx)
        return X, y

    def im_show(self, idx):
        input_im = self.get_input_im(idx)
        plt.imshow(input_im)

    def legion_show(self, idx):
        target_im = self.get_legion_im(idx)
        plt.imshow(target_im)


class UNet(nn.Module):
    def __init__(self):
        super(UNet, self).__init__()

        def blue(in_dim, out_dim):
            return [nn.Conv2d(in_dim, out_dim, kernel_size=(3, 3), stride=(1, 1), padding=1),
                    # nn.BatchNorm2d(out_dim),
                    nn.ReLU()]

        def red():
            return [nn.MaxPool2d(2)]

        def teal(in_dim):
            return [nn.Conv2d(in_dim, 1, kernel_size=(1, 1), stride=(1, 1))]
            # nn.BatchNorm2d(1)]

        def green(in_dim, out_dim):
            return [torch.nn.ConvTranspose2d(in_dim, out_dim, kernel_size=(2, 2), stride=(2, 2))]

        def grey(x, crop_size):
            return F.interpolate(x, crop_size)

        self.red1 = nn.Sequential(*red())
        self.red2 = nn.Sequential(*red())
        self.red3 = nn.Sequential(*red())
        self.green1 = nn.Sequential(*green(256, 256))
        self.green2 = nn.Sequential(*green(128, 128))
        self.green3 = nn.Sequential(*green(64, 64))

        self.crop1 = lambda x: grey(x, [16, 16])
        self.crop2 = lambda x: grey(x, [24, 24])
        self.crop3 = lambda x: grey(x, [40, 40])

        layers = blue(1, 64)
        layers.extend(blue(64, 64))
        self.block1 = nn.Sequential(*layers)

        layers = blue(64, 128)
        layers.extend(blue(128, 128))
        self.block2 = nn.Sequential(*layers)

        layers = blue(128, 256)
        layers.extend(blue(256, 256))
        self.block3 = nn.Sequential(*layers)

        layers = blue(256, 512)
        layers.extend(blue(512, 256))
        self.block4 = nn.Sequential(*layers)

        layers = blue(512, 256)
        layers.extend(blue(256, 128))
        self.block5 = nn.Sequential(*layers)

        layers = blue(256, 128)
        layers.extend(blue(128, 64))
        self.block6 = nn.Sequential(*layers)

        layers = blue(128, 64)
        layers.extend(blue(64, 64))
        layers.extend(teal(64))
        self.block7 = nn.Sequential(*layers)
        self.print = False

    def forward(self, x):
        print(10 * "*", "Downwards", 10 * "*") if self.print else print("", end="")
        print("Input shape:\n", x.shape) if self.print else print("", end="")
        x1 = self.block1(x)
        print("After blue convolutions:\n", x.shape) if self.print else print("", end="")
        x = self.red1(x1)
        print("After red max-pooling:\n", x.shape) if self.print else print("", end="")

        x2 = self.block2(x)
        print("After blue convolutions:\n", x.shape) if self.print else print("", end="")
        x = self.red2(x2)
        print("After red max-pooling:\n", x.shape) if self.print else print("", end="")

        x3 = self.block3(x)
        print("After blue convolutions:\n", x.shape) if self.print else print("", end="")
        x = self.red3(x3)
        print("After red max-pooling:\n", x.shape) if self.print else print("", end="")

        print("\n\n", 10 * "*", "Middle", 10 * "*") if self.print else print("", end="")
        x = self.block4(x)
        print("After blue convolutions:\n", x.shape) if self.print else print("", end="")
        x = self.green1(x)
        print("After green up-conv\n", x.shape) if self.print else print("", end="")

        print("\n\n", 10 * "*", "Upwards", 10 * "*") if self.print else print("", end="")
        print("Concat", x.shape, "and", x3.shape, "on dim=1") if self.print else print("", end="")
        x = torch.cat([x3, x], dim=1)
        print(x.shape) if self.print else print("", end="")
        x = self.block5(x)
        print("After blue convolutions:\n", x.shape) if self.print else print("", end="")
        x = self.green2(x)
        print("After green up-conv\n", x.shape) if self.print else print("", end="")

        print("Concat", x.shape, "and", x2.shape, "on dim=1") if self.print else print("", end="")
        x = torch.cat([x2, x], dim=1)
        print(x.shape) if self.print else print("", end="")
        x = self.block6(x)
        print("After blue convolutions:\n", x.shape) if self.print else print("", end="")
        x = self.green3(x)
        print("After green up-conv\n", x.shape) if self.print else print("", end="")

        print("Concat", x.shape, "and", x1.shape, "on dim=1") if self.print else print("", end="")
        x = torch.cat([x1, x], dim=1)
        print(x.shape) if self.print else print("", end="")
        x = self.block7(x)
        print("After blue convolutions:\n", x.shape) if self.print else print("", end="")
        return x


def focal_loss_q(y_real, y_pred,gamma=2):
    sigy = torch.clamp(torch.sigmoid(y_pred), 1e-6, 1-1e-6)
    loss = (1-sigy) ** gamma * torch.log(sigy) + (1-y_real) * torch.log(1-sigy)

    loss = -loss.mean()

    return loss


def train(model, opt, loss_fn, epochs, data_tr, val_loader, patience):
    train_loss_lst = []
    val_loss_lst = []
    patience_counter = 0
    best_val_loss = 1e15
    best_model_wts = copy.deepcopy(model.state_dict())
    for epoch in range(epochs):
        print('* Epoch %d/%d' % (epoch+1, epochs))

        avg_loss = 0
        model.train()  # train mode
        for X_batch, Y_batch in data_tr:
            X_batch = X_batch.to(device)
            Y_batch = Y_batch.to(device)

            # set parameter gradients to zero
            opt.zero_grad()

            # forward
            Y_pred = model(X_batch)
            loss = loss_fn(Y_pred, Y_batch)  # forward-pass
            loss.backward()  # backward-pass
            opt.step()  # update weights

            # calculate metrics to show the user
            avg_loss += loss / len(data_tr)
        print(' - loss: %f' % avg_loss)
        train_loss_lst.append(avg_loss)

        avg_loss_val = 0
        model.eval()  # testing mode
        for X_batch, Y_batch in val_loader:
            X_batch = X_batch.to(device)
            Y_batch = Y_batch.to(device)

            # forward
            with torch.set_grad_enabled(False):
                Y_pred = model(X_batch)
                loss = loss_fn(Y_pred, Y_batch)  # forward-pass
                avg_loss_val += loss / len(data_val)

        if avg_loss_val < best_val_loss:
            best_val_loss = avg_loss_val
            best_model_wts = copy.deepcopy(model.state_dict())
            torch.save(best_model_wts, f"Model Checkpoints/Unet_{str(loss_fn)}_lr{lr}.pth")
            patience_counter = 0
        else:
            patience_counter += 1

        val_loss_lst.append(avg_loss_val)
        print(' - val loss: %f' % avg_loss_val)
        if patience_counter > patience:
            print("Early stopping after", epoch, "epochs")
            break

    return train_loss_lst, val_loss_lst, best_model_wts


batch_size = 32
epochs = 300
patience = 5
lr = 1e-5
transform = transforms.Compose([transforms.ToTensor()])

data_train = LIDC_IDRI(transform, mode='train', legions=[0])
train_loader = DataLoader(data_train, batch_size=batch_size, shuffle=True, num_workers=0)
data_val = LIDC_IDRI(transform, mode='val', legions=[0])
val_loader = DataLoader(data_val, batch_size=batch_size, shuffle=True, num_workers=0)

num_ones = 0.0
num_zeros = 0.0
for inputs, labels in train_loader:
    num_ones += torch.sum(labels == 1)
    num_zeros += torch.sum(labels == 0)
train_weight = num_zeros / num_ones

net = UNet()
net.to(device)
# Load from last checkpoint (LAST TIME LR = 1e-3)
net.load_state_dict(torch.load(f"Model Checkpoints/Unet_BCEWithLogitsLoss()_lr0.0001.pth"))
opt = torch.optim.Adam(net.parameters(), lr=lr)
loss_fn = torch.nn.BCEWithLogitsLoss(pos_weight=train_weight)
train_loss_lst, val_loss_lst, best_model = train(net, opt, loss_fn, epochs, train_loader, val_loader, patience)

np.savetxt("train_loss.txt", np.array(train_loss_lst))
np.savetxt("val_loss.txt", np.array(val_loss_lst))
