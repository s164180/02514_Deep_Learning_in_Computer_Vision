import torch
td = torch.distributions
from math import pi

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class VariationalPosterior(object):
    def __init__(self, mu, rho):
        super().__init__()
        self.mu = mu
        self.rho = rho
        self.stdNorm = td.Normal(0,1)

    @property
    def sigma(self):
        return torch.nn.Softplus()(self.rho)

    def sample(self, training, sampling=True):
        if training:
            epsilon = self.stdNorm.sample(self.rho.shape).to(device)
            return self.mu + self.sigma*epsilon
        elif sampling:
            samp = td.Normal(self.mu, self.sigma).sample()
            assert ((torch.sum(torch.isnan(samp)) == 0) & (torch.sum(torch.isinf(samp)) == 0))
            return samp
        else:
            return self.mu

    def log_prob(self, x):
        return torch.sum(- torch.log(torch.sqrt(2*torch.tensor(pi)))
                         - torch.log(self.sigma) - ((x - self.mu)**2) / (2*self.sigma**2))
