import torch
td = torch.distributions
from distributions import VariationalPosterior
import torch.nn.functional as F
from typing import Optional


import torch.nn as nn
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


class BayesianVgg11(nn.Module):
    def __init__(self, filters, input_size):
        super(BayesianVgg11, self).__init__()

        self.convolutional1 = nn.Sequential(
            BayesianConv2d(3, filters, (3, 3), padding=0),
            nn.BatchNorm2d(filters, eps=1e-2),
            nn.ReLU(),
            nn.MaxPool2d(2),
        )

        self.convolutional2 = nn.Sequential(
            BayesianConv2d(filters, 2 * filters, (3, 3), padding=0),
            nn.BatchNorm2d(2 * filters, eps=1e-2),
            nn.ReLU(),
            nn.MaxPool2d(2),
        )

        self.convolutional3_1 = nn.Sequential(
            BayesianConv2d(2 * filters, 4 * filters, (3, 3), padding=0),
            nn.BatchNorm2d(4 * filters, eps=1e-2),
            nn.ReLU(),
        )

        self.convolutional3_2 = nn.Sequential(
            BayesianConv2d(4 * filters, 4 * filters, (3, 3), padding=0),
            nn.BatchNorm2d(4 * filters, eps=1e-2),
            nn.ReLU(),
            nn.MaxPool2d(2),
        )

        self.convolutional4_1 = nn.Sequential(
            BayesianConv2d(4 * filters, 8 * filters, (3, 3), padding=0),
            nn.BatchNorm2d(8 * filters, eps=1e-2),
            nn.ReLU(),
        )

        self.convolutional4_2 = nn.Sequential(
            BayesianConv2d(8 * filters, 8 * filters, (3, 3), padding=0),
            nn.BatchNorm2d(8 * filters, eps=1e-2),
            nn.ReLU(),

            nn.MaxPool2d(2),
        )

        self.convolutional5 = nn.Sequential(
            BayesianConv2d(8 * filters, 8 * filters, (3,3), padding=0),
            nn.BatchNorm2d(8 * filters, eps=1e-2),
            nn.ReLU(),

            BayesianConv2d(8 * filters, 8 * filters, (3,3), padding=0),
            nn.BatchNorm2d(8 * filters, eps=1e-2),
            nn.ReLU(),

            nn.MaxPool2d(2)
        )
        spatial_dim = int(input_size / 2 ** 5)

        self.convolutions = [self.convolutional1, self.convolutional2, self.convolutional3_1,
                             self.convolutional3_2, self.convolutional4_1, self.convolutional4_2,
                             self.convolutional5]

        self.fully_connected = nn.Sequential(
            nn.Linear(8 * filters * spatial_dim * spatial_dim, 64 * filters),
            nn.ReLU(),
            nn.Dropout(),

            nn.Linear(64 * filters, 64 * filters),
            nn.ReLU(),
            nn.Dropout(),

            nn.Linear(64 * filters, 2),
            nn.LogSoftmax(dim=1))

    def switch_to_training(self, to_train):
        for conv in self.convolutions:
            for layer in conv:
                if isinstance(layer, BayesianConv2d):
                    layer.training = to_train

    def switch_to_sampling(self, to_sample):
        for conv in self.convolutions:
            for layer in conv:
                if isinstance(layer, BayesianConv2d):
                    layer.sampling = to_sample


    def forward(self, x):
        x = self.convolutional1(x)
        x = self.convolutional2(x)
        x = self.convolutional3_1(x)
        x = self.convolutional3_2(x)
        x = self.convolutional4_1(x)
        x = self.convolutional4_2(x)
        x = self.convolutional5(x)
        x = x.view(x.size(0), -1)
        x = self.fully_connected(x)
        return x


class _ConvNd(nn.Module):

    __constants__ = ['stride', 'padding', 'dilation', 'groups',
                     'padding_mode', 'output_padding', 'in_channels',
                     'out_channels', 'kernel_size']
    __annotations__ = {'bias': Optional[torch.Tensor]}

    def __init__(self, in_channels, out_channels, kernel_size, stride,padding, dilation, transposed, output_padding,
                 groups, bias, padding_mode):
        super(_ConvNd, self).__init__()

        if in_channels % groups != 0:
            raise ValueError('in_channels must be divisible by groups')
        if out_channels % groups != 0:
            raise ValueError('out_channels must be divisible by groups')
        valid_padding_modes = {'zeros', 'reflect', 'replicate', 'circular'}
        if padding_mode not in valid_padding_modes:
            raise ValueError("padding_mode must be one of {}, but got padding_mode='{}'".format(
                valid_padding_modes, padding_mode))
        self.in_channels = in_channels
        self.out_channels = out_channels
        self.kernel_size = kernel_size
        self.stride = stride
        self.padding = padding
        self.dilation = dilation
        self.transposed = transposed
        self.output_padding = output_padding
        self.groups = groups
        self.padding_mode = padding_mode
        self._padding_repeated_twice = (self.padding, self.padding)
        if transposed:
            #self.weight = nn.Parameter(torch.Tensor(
            #    in_channels, out_channels // groups, kernel_size))
            pass
        else:
            self.weight_mu = nn.Parameter(torch.Tensor(out_channels // groups, in_channels,  kernel_size[0], kernel_size[1]))
            self.weight_rho = nn.Parameter(torch.Tensor(out_channels // groups, in_channels, kernel_size[0], kernel_size[1]))
            self.weight_dist = VariationalPosterior(self.weight_mu, self.weight_rho)
        if bias:
            self.bias = nn.Parameter(torch.Tensor(out_channels))
        else:
            self.register_parameter('bias', None)
        self.reset_parameters()

    def reset_parameters(self):
        # nn.init.uniform_(self.weight_mu, -1e-1, 1e-1)
        nn.init.zeros_(self.weight_mu)
        # nn.init.uniform_(self.weight_rho, -3, -2)
        nn.init.zeros_(self.weight_rho)

    def __setstate__(self, state):
        super(_ConvNd, self).__setstate__(state)
        if not hasattr(self, 'padding_mode'):
            self.padding_mode = 'zeros'


class BayesianConv2d(_ConvNd):
    def __init__(self, in_channels, out_channels, kernel_size, stride=1,
                 padding=0, dilation=1, groups=1, bias=True, padding_mode='zeros'):
        super(BayesianConv2d, self).__init__(in_channels, out_channels, kernel_size, stride, padding, dilation, False,
                                             0, groups, bias, padding_mode)
        self.training = None
        self.sampling = None

    def _conv_forward(self, input, weight):

        if self.padding_mode != 'zeros':
            return F.conv2d(F.pad(input, self._padding_repeated_twice, mode=self.padding_mode),
                            weight, self.bias, self.stride,0, self.dilation, self.groups)

        return F.conv2d(input, weight, self.bias, self.stride, self.padding, self.dilation, self.groups)

    def forward(self, input):
        self.weight = self.weight_dist.sample(self.training, self.sampling)
        print("conv is inf", torch.sum(torch.isinf(self._conv_forward(input, self.weight))))
        print("conv is nan", torch.sum(torch.isnan(self._conv_forward(input, self.weight))))
        return self._conv_forward(input, self.weight)


class BayesianLSTMCellTied(nn.Module):

    def __init__(self, num_units, training, init, prior, state_size, **kwargs):
        super(BayesianLSTMCellTied, self).__init__(num_units, **kwargs)
        self.init = init
        self.prior = prior
        self.units = num_units
        self.state_size = num_units
        self.is_training = training
        self.num_links = state_size
        self.W_mu = nn.Parameter(torch.Tensor(self.units + self.num_links, 4 * self.units))
        self.W_rho = nn.Parameter(torch.Tensor(self.units + self.num_links, 4 * self.units))
        self.B_mu = nn.Parameter(torch.Tensor(1, 4 * self.units))
        self.B_rho = nn.Parameter(torch.Tensor(1, 4 * self.units))
        self.W_dist = VariationalPosterior(self.W_mu, self.W_rho)
        self.B_dist = VariationalPosterior(self.B_mu, self.B_rho)

        print("  Tied Cell has been built (in:", self.num_links, ") (out:", self.units, ")")
        self.sampling = False
        self.built = True

    def call(self, inputs, states):
        W = self.W_dist.sample(self.is_training, self.sampling)
        B = self.B_dist.sample(self.is_training, self.sampling)
        c_t, h_t = torch.split(states[0], 2, dim=0)
        concat_inputs_hidden = torch.cat([inputs, h_t], 1)
        concat_inputs_hidden = torch.matmul(concat_inputs_hidden, W.squeeze()) + B.squeeze()

        self.log_prior = torch.sum(self.prior.log_prob(W)) + torch.sum(self.prior.log_prob(B))
        self.log_variational_posterior = torch.sum(self.W_dist.log_prob(W)) + torch.sum(self.B_dist.log_prob(B))

        # Gates: Input, New, Forget and Output
        i, j, f, o = torch.split(concat_inputs_hidden, 4, dim=1)
        c_new = c_t * torch.sigmoid(f) + torch.sigmoid(i) * torch.math.tanh(j)
        h_new = torch.tanh(c_new) * torch.sigmoid(o)
        new_state = torch.cat([c_new, h_new], dim=0)
        return h_new, new_state

    def get_initial_state(self, inputs=None, batch_size=None, dtype=None):
        return torch.zeros((2 * batch_size, self.units), dtype=dtype)