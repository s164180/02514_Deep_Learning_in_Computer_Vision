
## Load functions

from argparse import ArgumentParser
import torch
from torch.utils.data import DataLoader
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torchsummary import summary
import torchvision.transforms as transforms
import os

# custom packages
from models import UNet2
from utils import LIDC, train, focal_loss, bce_loss


# Read arguments
parser = ArgumentParser()
parser.add_argument('--lr', default=0.000001, type=float)
parser.add_argument('--batch_size', default=32, type = int)
parser.add_argument('--epochs', default=30, type = int)
parser.add_argument('--num_workers', default=12)
parser.add_argument('--CUDA_VISIBLE_DEVICES', default = 0)
parser.add_argument('--name', default = 'qahir')
parser.add_argument('--loss_fn', default='focal')
parser.add_argument('--lesion', default = '0')


args = parser.parse_args()

os.environ["CUDA_VISIBLE_DEVICES"]="{%s}" % (args.CUDA_VISIBLE_DEVICES)


device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print(device)

# check model
model = UNet2(lesion = args.lesion)
model.to(device)

summary(model, (1, 128, 128))

### define train, test, val data loader
batch_size = args.batch_size

#transform = transforms.Compose([transforms.RandomHorizontalFlip(),transforms.RandomRotation(45),transforms.RandomVerticalFlip(),transforms.ToTensor()])
transform = transforms.ToTensor()
train_data = LIDC(mode='train',transform=transform,lesion=args.lesion)
val_data = LIDC(mode='val',lesion=args.lesion)
test_data = LIDC(mode='test',lesion=args.lesion)


data_tr = DataLoader(train_data, batch_size=batch_size, shuffle=True,num_workers=12)
data_val = DataLoader(val_data, batch_size=batch_size, shuffle=False,num_workers=12)
data_ts = DataLoader(test_data, batch_size=batch_size, shuffle=False,num_workers=12)

opt = optim.Adam(model.parameters(), lr = args.lr)
loss_fn = None

if args.loss_fn == 'bce':
    loss_fn = bce_loss

if args.loss_fn =='focal':
    loss_fn = focal_loss

epochs = args.epochs

hparms = {
    'lr' : args.lr,
    'batch_size': args.batch_size,
    'epochs': args.epochs,
    'num_workers': args.num_workers,
    'GPU': args.CUDA_VISIBLE_DEVICES,
    'lesion': int(args.lesion)
}

print(args.__dict__)

writer = SummaryWriter()
writer.add_hparams(args.__dict__,{})
train(model, opt, loss_fn, epochs, data_tr, data_val,device,writer)
torch.save(model.state_dict(), 'models/' + args.name + '.p')