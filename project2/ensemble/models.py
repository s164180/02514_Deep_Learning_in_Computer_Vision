
## Load functions


import torch
import matplotlib.pyplot as plt
import os

from PIL import Image
import torch
import torchvision.transforms as transforms

from torch.utils.data import DataLoader

import torch.nn as nn
import torch.nn.functional as F
from torchvision import models
from torchsummary import summary
import torch.optim as optim
from time import time
import numpy as np
# pip install torchsummary
from tqdm import trange, tqdm
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


# U-net
class UNet2(nn.Module):
    def __init__(self,lesion = '0'):
        super().__init__()

        self.lesion = lesion

        # encoder (downsampling)
        self.enc_conv0 = nn.Conv2d(1, 64, 3, padding=1)
        self.pool0 = nn.MaxPool2d(2)
        self.enc_conv1 = nn.Conv2d(64, 128, 3, padding=1)
        self.pool1 = nn.MaxPool2d(2)
        self.enc_conv2 = nn.Conv2d(128, 256, 3, padding = 1)
        self.pool2 = nn.MaxPool2d(2)

        #self.enc_conv3 = nn.Conv2d(256, 512, 3, padding = 1)
        #self.pool3 = nn.Conv2d(512,512, kernel_size = 2, stride = 2)

        # bottleneck
        self.bottleneck_conv = nn.Conv2d(256, 512, 3, padding =1)

        # decoder (upsampling)
        self.upsample0 = nn.ConvTranspose2d(512,256,kernel_size=2,stride=2)
        self.dec_conv0 = nn.Conv2d(256+256, 256, 3, padding=1)
        self.upsample1 = nn.ConvTranspose2d(256,128,kernel_size=2,stride=2) # 64 -> 128
        self.dec_conv1 = nn.Conv2d(128+128, 128, 3, padding=1)
        self.upsample2 = nn.ConvTranspose2d(128,64,kernel_size=2,stride=2) # 64 -> 128
        self.dec_conv2 = nn.Conv2d(64+64, 64, 3, padding=1)
        self.upsample3 = nn.ConvTranspose2d(64,32,kernel_size=2,stride=2) # 64 -> 128

        self.dec_conv3 = nn.Conv2d(64, 1, 3, padding=1)

    def forward(self, x):
        #encoder
        e0 = F.relu(self.enc_conv0(x))
        e1 = F.relu(self.enc_conv1(self.pool0(e0)))
        e2 = F.relu(self.enc_conv2(self.pool1(e1)))
        b = F.relu(self.bottleneck_conv(self.pool2(e2)))

        # decoder
        d0 = F.relu(self.dec_conv0(torch.cat([self.upsample0(b),e2],1)))

        d1 = F.relu(self.dec_conv1(torch.cat([self.upsample1(d0),e1],1)))
        d2 = F.relu(self.dec_conv2(torch.cat([self.upsample2(d1),e0],1)))
        d3 = self.dec_conv3(d2)
        return d3
