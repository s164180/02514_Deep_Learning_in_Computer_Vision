

## Load functions


import torch
import matplotlib.pyplot as plt
import os

from PIL import Image
import torch
import torchvision.transforms as transforms

from torch.utils.data import DataLoader

import torch.nn as nn
import torch.nn.functional as F
from torchvision import models
from torchsummary import summary
import torch.optim as optim
from time import time
import numpy as np
# pip install torchsummary
from tqdm import tqdm

device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

# Dataloader
class LIDC(torch.utils.data.Dataset):
    def __init__(self, data_path = './LIDC_crops/LIDC_DLCV_version/', mode = 'train', transform = transforms.ToTensor() ,lesion = '0'):
        'Initialization'
        self.lesion = lesion

        self.data_path = data_path + mode

        self.images = []

        self.transform = transform

        for root,dirs, files in os.walk(self.data_path):
            if root.endswith('images'):
                for file in files:
                    self.images.append(os.path.join(root, file))

    def __len__(self):
        'Returns length of dataset'
        return len(self.images)

    def __getitem__(self,idx):
        X = Image.open(self.images[idx])
        X = self.transform(X)

        # Lesion path. We only use the first annotation for the first 4 questions
        l_path = self.images[idx][:-4] + '_l' + self.lesion + '.png'
        l_path = l_path.replace('images','lesions')
        y = self.transform(Image.open(l_path))

        return X, y

# Train module
def train(model, opt, loss_fn, epochs, data_tr, data_val,device,writer):
    X_val, Y_val = next(iter(data_val))
    for epoch in tqdm(range(epochs), desc='epoch'):
        train_loss = 0
        model.train()  # train mode
        for X_batch, Y_batch in data_tr:
            X_batch = X_batch.to(device)
            Y_batch = Y_batch.to(device)

            # set parameter gradients to zero
            opt.zero_grad()

            # forward
            Y_pred = model(X_batch)
            loss = loss_fn(Y_batch, Y_pred)  # forward-pass
            loss.backward()  # backward-pass
            opt.step()  # update weights

            # calculate metrics to show the user
            train_loss += loss

        train_loss = train_loss / len(data_tr)
        print('tran loss: ', train_loss)
        writer.add_scalar('Loss/train', train_loss, epoch)

        # show intermediate results

        with torch.no_grad():
            model.eval()  # testing mode
            val_loss = 0

            for X_batch, Y_batch in data_tr:
                X_batch = X_batch.to(device)
                Y_batch = Y_batch.to(device)

                # forward
                Y_pred = model(X_batch)
                loss = loss_fn(Y_batch, Y_pred)  # forward-pass
                val_loss += loss

            val_loss = val_loss / len(data_val)
            print('val_loss: ', val_loss)
            writer.add_scalar('Loss/val', val_loss,epoch)

            # Get train loss
            Y_hat = torch.sigmoid(model(X_val.to(device))).detach().cpu()
            fig = plt.figure()
            for k in range(6):
                plt.subplot(3, 6, k+1)
                plt.imshow(X_val[k].squeeze(), cmap='gray')
                plt.title('Real')
                plt.axis('off')

                plt.subplot(3, 6, k+7)
                plt.imshow(Y_val[k].squeeze(), cmap='gray')
                plt.title('Ann')
                plt.axis('off')

                plt.subplot(3, 6, k+13)
                plt.imshow(Y_hat[k].squeeze(), cmap='gray')
                plt.title('Output')
                plt.axis('off')
            plt.suptitle('%d / %d - loss train: %f m loss val: %f' % (epoch+1, epochs, train_loss,val_loss))
            writer.add_figure("matplotlib/figure", fig)


#### Loss functions
def focal_loss(y_real, y_pred,gamma=2):
    sigy = torch.clamp(torch.sigmoid(y_pred), 1e-6, 1-1e-6)
    loss = (1-sigy) ** gamma * torch.log(sigy) + (1-y_real) * torch.log(1-sigy)
    loss = -loss.mean()
    return loss

def bce_total_variation(y_real, y_pred):
    y_sig = torch.sigmoid(y_pred)
    total_variation = (y_sig[:,:,1:,:]- y_sig[:,:,:-1,:]).mean() + (y_sig[:,:,:,1:]  - y_sig[:,:,:,:-1]).mean()
    return bce_loss(y_real, y_pred) + total_variation

def bce_loss(y_real, y_pred):
    return torch.mean(y_pred - y_real*y_pred + torch.log(1 + torch.exp(-y_pred)))


### Metrics

def dice_loss(y_real, y_pred):
    # Use sigmoid function on predictions to squeeze into [0,1]
    smooth = 1
    y_pred = torch.sigmoid(y_pred)

    y_real = y_real.view(-1)
    y_pred = y_pred.view(-1)
    num = 2 * y_real * y_pred
    denom  = y_real + y_pred
    loss =  (num/denom).mean() # Do it this way to take dimension into account
    loss = 1 - loss
    return loss

#def IOU(y_real, y_pred):

def accuracy(y_real, y_pred):
    n_cor = y_real == y_pred
    return np.mean(n_cor)

def sensitivity(y_real, y_pred):
    TPR = y_real * y_pred
    return TPR.sum() / y_real.sum()

def specificity(y_real, y_pred):
    return 0
