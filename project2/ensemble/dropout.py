## Load functions

from argparse import ArgumentParser
import torch
from torch.utils.data import DataLoader
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torchsummary import summary
import torchvision.transforms as transforms
import os

# custom packages
from models import SegNet, UNet2
from utils import LIDC, train, focal_loss, bce_loss
import matplotlib.pyplot as plt



#### Load and run 50 different dropout examples

model = UNet2()
state_dict = torch.load('models/qahir.p', map_location=torch.device('cpu'))
model.load_state_dict(state_dict)

# Make dropout active
model.train()


batch_size = 6
val_data = LIDC(mode='val')
test_data = LIDC(mode='test')
data_val = DataLoader(val_data, batch_size=batch_size, shuffle=False,num_workers=12)
data_ts = DataLoader(test_data, batch_size=batch_size, shuffle=False,num_workers=12)
X_test, Y_test = next(iter(data_ts))


# forward
Y_pred = model(X_test).sigmoid()

for i in range(10):
    print(i)
    Y_pred += model(X_test).sigmoid()

Y_pred = Y_pred / 50

fig = plt.figure()
for k in range(6):
    plt.subplot(3, 6, k + 1)
    plt.imshow(X_test[k].squeeze(), cmap='gray')
    plt.title('Real')
    plt.axis('off')

    plt.subplot(3, 6, k + 7)
    plt.imshow(Y_test[k].squeeze(), cmap='gray')
    plt.title('Ann')
    plt.axis('off')

    plt.subplot(3, 6, k + 13)
    plt.imshow(Y_pred[k].squeeze().detach(), cmap='gray')
    plt.title('Output')
    plt.axis('off')

plt.colorbar()
plt.show()