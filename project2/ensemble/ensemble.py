## Load functions

from argparse import ArgumentParser
import torch
from torch.utils.data import DataLoader
import torch.optim as optim
from torch.utils.tensorboard import SummaryWriter
from torchsummary import summary
import torchvision.transforms as transforms
import os

# custom packages
from models import UNet2
from utils import LIDC, train, focal_loss, bce_loss
import matplotlib.pyplot as plt



#### Load and run 50 different dropout examples

model0 = UNet2()
state_dict = torch.load('models/lesion0.p', map_location=torch.device('cpu'))
model0.load_state_dict(state_dict)

model1 = UNet2()
state_dict = torch.load('models/lesion1.p', map_location=torch.device('cpu'))
model1.load_state_dict(state_dict)


model2 = UNet2()
state_dict = torch.load('models/lesion2.p', map_location=torch.device('cpu'))
model2.load_state_dict(state_dict)

model3 = UNet2()
state_dict = torch.load('models/lesion3.p', map_location=torch.device('cpu'))
model3.load_state_dict(state_dict)


models = [model0, model1, model2, model3]

batch_size = 6
val_data = LIDC(mode='val',lesion='1')
test_data = LIDC(mode='test',lesion='1')
data_val = DataLoader(val_data, batch_size=batch_size, shuffle=False,num_workers=12)
data_ts = DataLoader(test_data, batch_size=batch_size, shuffle=False,num_workers=12)

X_test, Y_test = next(iter(data_val))


Y_pred = []
Y_p = Y_test * 0

# Make dropout active
for model in models:
    p = model(X_test).sigmoid()
    Y_pred.append(p)
    Y_p += p


Y_p = Y_p / 4

fig = plt.figure()
for k in range(6):
    plt.subplot(3, 6, k + 1)
    plt.imshow(X_test[k].squeeze(), cmap='gray')
    plt.title('Real')
    plt.axis('off')

    plt.subplot(3, 6, k + 7)
    plt.imshow(Y_test[k].squeeze(), cmap='gray')
    plt.title('An.')
    plt.axis('off')

    plt.subplot(3, 6, k + 13)
    plt.imshow(Y_p[k].squeeze().detach(), cmap='gray')
    plt.title('Ens.')
    plt.axis('off')

plt.tight_layout()
plt.savefig('ens1.eps')

fig = plt.figure()
for k in range(6):
    plt.subplot(4, 6, k + 1)
    plt.imshow(Y_pred[0][k].squeeze().detach(), cmap='gray')
    plt.title('Net1')
    plt.axis('off')

    plt.subplot(4, 6, k + 7)
    plt.imshow(Y_pred[1][k].squeeze().detach(), cmap='gray')
    plt.title('Net2')
    plt.axis('off')

    plt.subplot(4, 6, k + 13)
    plt.imshow(Y_pred[2][k].squeeze().detach(), cmap='gray')
    plt.title('Net3')
    plt.axis('off')

    plt.subplot(4, 6, k + 19)
    plt.imshow(Y_pred[3][k].squeeze().detach(), cmap='gray')
    plt.title('Net4')
    plt.axis('off')

plt.tight_layout()
plt.savefig('ens2.eps')