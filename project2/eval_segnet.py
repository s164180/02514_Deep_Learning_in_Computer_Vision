from torchvision import transforms
from torch.utils.data import DataLoader
import torch
import copy
from PIL import Image
import matplotlib.pyplot as plt
import torch.nn.functional as F
import torch.nn as nn
from pathlib import Path
import numpy as np
import json
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


# test = 'IoU' or 'Dice'
def Loss(y_test, y_pred, test):
    h = 128
    w = 128

    intersection = np.sum(np.abs(y_pred * y_test))
    union = np.sum(np.abs(y_test)) + np.sum(np.abs(y_pred)) - intersection
    if union == 0:
        IoU = 0
    else:
        IoU = intersection / union
    if union + intersection == 0:
        Dice = 0
    else:
        Dice = 2 * intersection / (union + intersection)

    if test == 'IoU':
        return IoU
    if test == 'Dice':
        return Dice


def Accuracy(y_test, y_pred):
    h = 128
    w = 128

    pixel_total = h * w
    correct = 0

    # Looping over each pixel in the true and the predicted image
    for i in range(y_test.shape[0]):
        for j in range(y_test.shape[1]):
            if y_test[i, j] == y_pred[i, j]:
                correct += 1

    return correct / pixel_total


# test = 'Sensitivity' or 'Specificity'
def SenSpe(y_test, y_pred, test):
    TP = 0
    FN = 0
    TN = 0
    FP = 0

    if test == 'Sensitivity':
        for i in range(y_test.shape[0]):
            for j in range(y_test.shape[1]):

                if y_test[i, j] == y_pred[i, j] and y_test[i, j] == 1 and y_pred[i, j] == 1:
                    TP += 1
                if y_test[i, j] != y_pred[i, j] and y_test[i, j] == 1 and y_pred[i, j] == 0:
                    FN += 1
        if TP + FN == 0:
            sensitivity = 1
        else:
            sensitivity = TP / (TP + FN)

    if test == 'Specificity':
        for i in range(y_test.shape[0]):
            for j in range(y_test.shape[1]):

                if y_test[i, j] == y_pred[i, j] and y_test[i, j] == 0 and y_pred[i, j] == 0:
                    TN += 1

                if y_test[i, j] != y_pred[i, j] and y_test[i, j] == 0 and y_pred[i, j] == 1:
                    FP += 1
        if TN + FP == 0:
            specificity = 1
        else:
            specificity = TN / (TN + FP)

    if test == 'Sensitivity':
        return sensitivity
    if test == 'Specificity':
        return specificity


class LIDC_IDRI(torch.utils.data.Dataset):
    def __init__(self, transform, mode, legions, data_path='LIDC_crops/LIDC_DLCV_version'):
        'Initialization'
        self.transform = transform
        data_path += "/" + mode
        print(data_path)
        self.image_paths = [data_path + "/images/" + path.name for path in Path(data_path + "/images").rglob('*')]

        all_target_paths = [data_path + "/lesions/" + path.name for path in Path(data_path + "/lesions").rglob('*')]

        self.target_paths = []
        for legion in legions:
            legion_path = [path for path in all_target_paths if path.endswith(f"l{legion}.png")]
            self.target_paths.append(legion_path)
        im = Image.open(self.image_paths[0])
        X = self.transform(im)
        self.dim = X.shape[1]

    def __len__(self):
        return len(self.image_paths)

    def get_input_im(self, idx):
        return Image.open(self.image_paths[idx])

    def get_legion_im(self, idx):
        if len(self.target_paths) == 1:
            return Image.open(self.target_paths[0][idx])
        else:
            return None

    def get_x_tensor(self, idx):
        input_im = self.get_input_im(idx)
        return self.transform(input_im)

    def get_y_tensor(self, idx):
        if len(self.target_paths) == 1:
            target_im = self.get_legion_im(idx)
            return self.transform(target_im)
        else:
            return None

    def __getitem__(self, idx):
        X = self.get_x_tensor(idx)
        y = self.get_y_tensor(idx)
        return X, y

    def im_show(self, idx):
        input_im = self.get_input_im(idx)
        plt.imshow(input_im)

    def legion_show(self, idx):
        target_im = self.get_legion_im(idx)
        plt.imshow(target_im)


class SegNet(nn.Module):
    def __init__(self):
        super().__init__()

        # Encoder (downsampling)

        # 128 -> 64
        self.encoder_convolution0 = nn.Conv2d(1, 64, 3, padding=1)
        self.pool0 = nn.MaxPool2d(3, 2, padding=1)

        # 64 -> 32
        self.encoder_convolution1 = nn.Conv2d(64, 64, 3, padding=1)
        self.pool1 = nn.MaxPool2d(3, 2, padding=1)

        # 32 -> 16
        self.encoder_convolution2 = nn.Conv2d(64, 64, 3, padding=1)
        self.pool2 = nn.MaxPool2d(3, 2, padding=1)

        # Bottleneck
        self.bottleneck_conv = nn.Conv2d(64, 64, 3, padding=1)

        # Decoder (upsampling)

        # 16 -> 32
        self.upsample0 = nn.Upsample(32)
        self.decoder_convolution0 = nn.Conv2d(64, 64, 3, padding=1)

        # 32 -> 64
        self.upsample1 = nn.Upsample(64)
        self.decoder_convolution1 = nn.Conv2d(64, 64, 3, padding=1)

        # 64 -> 128
        self.upsample2 = nn.Upsample(128)
        self.decoder_convolution2 = nn.Conv2d(64, 1, 3, padding=1)

    def forward(self, x):
        # Encoder
        e0 = self.pool0(F.relu(self.encoder_convolution0(x)))
        e1 = self.pool1(F.relu(self.encoder_convolution1(e0)))
        e2 = self.pool2(F.relu(self.encoder_convolution2(e1)))

        # Bottleneck
        b = F.relu(self.bottleneck_conv(e2))

        # Decoder
        d0 = F.relu(self.decoder_convolution0(self.upsample0(b)))
        d1 = F.relu(self.decoder_convolution1(self.upsample1(d0)))
        d2 = self.decoder_convolution2(self.upsample2(d1))  # no activation of the last one
        return d2


transform = transforms.Compose([transforms.ToTensor()])

data_test = LIDC_IDRI(transform, mode='val', legions=[0])
test_loader = DataLoader(data_test, batch_size=1, shuffle=True, num_workers=0)

# Load
net = SegNet()
net.to(device)
net.load_state_dict(torch.load("Model Checkpoints/SegNet_BCEWithLogitsLoss()_lr0.0001.pth"))

avg_loss_val = 0
net.eval()  # testing mode
y_pred = []
for X_batch, Y_batch in test_loader:
    X_batch = X_batch.to(device)
    Y_batch = Y_batch.to(device)

    # forward
    outputs = net(X_batch)
    y_pred.append(torch.sigmoid(outputs).cpu().detach().numpy())

results_dict = {}
p_thresh_range = [0.8, 0.9]

## CHange to validation
for p_thresh in p_thresh_range:
    results = {}
    iou = 0
    dice = 0
    acc = 0
    sens = 0
    spec = 0
    for i in range(len(y_pred)):
        y_pred_sample = (np.squeeze(y_pred[i]) > p_thresh).astype(int)
        y_test_sample = np.squeeze(data_test.get_y_tensor(i).numpy())
        # Loss ('IoU' or 'Dice'), Accuracy, SenSpe ('Sensitivity' or 'Specificity')
        iou += Loss(y_pred_sample, y_test_sample, 'IoU') / len(y_pred)
        dice += Loss(y_pred_sample, y_test_sample, 'Dice') / len(y_pred)
        acc += Accuracy(y_test_sample, y_pred_sample) / len(y_pred)
        sens += SenSpe(y_test_sample, y_pred_sample, 'Sensitivity') / len(y_pred)
        spec += SenSpe(y_test_sample, y_pred_sample, 'Specificity') / len(y_pred)
    results['IoU'] = iou
    results['Dice'] = dice
    results['Accuracy'] = acc
    results['Sensitivity'] = sens
    results['Specificity'] = spec
    results_dict[p_thresh] = results

## Choose a threshold for test set

res = json.dumps(results_dict)
f = open("results_segnet.json", "w")
f.write(res)
f.close()
