import torch
import torch.nn as nn
from train_model import train_model
from data import load_data
from test_model import test_classifier
import json
from torchvision.models import vgg11, resnet18, squeezenet1_0


models = [resnet18, squeezenet1_0, vgg11]
model_names = ["resnet18_pretrained", "squeezenet1_0_pretrained", "vgg11_pretrained"]
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

for model_name, mod_construct in zip(model_names, models):
    input_size = 224
    batch_size = 32
    model = mod_construct(pretrained=True)
    model.to(device)
    for param in model.parameters():
        param.requires_grad = False
    # For the last layer of the classifier change the output size to 2 (hotdog or not)
    if model_name == "resnet18_pretrained":
        tens_lst = []
        for param in model.avgpool.parameters():
            param.requires_grad = True
            tens_lst.append(param)
        for param in model.fc.parameters():
            param.requires_grad = True
            tens_lst.append(param)
        model.fc.out_features = 2
        optimizer = torch.optim.SGD(tens_lst, lr=1e-3, momentum=0.9)
    else:
        if model_name == "squeezenet1_0_pretrained":
            model.classifier[1].out_features = 2
        else:
            model.classifier[6].out_features = 2
        # We only train the classifier part
        for param in model.classifier.parameters():
            param.requires_grad = True
        optimizer = torch.optim.SGD(model.classifier.parameters(), lr=1e-3, momentum=0.9)
    loss = nn.CrossEntropyLoss()


    train_loader, val_loader, test_loader = load_data(batch_size, input_size)
    dataloaders_dict = {'train': train_loader, 'val': val_loader}
    model, val_loss, val_acc = train_model(model, dataloaders_dict, loss, optimizer, num_epochs=100, patience=5)


    print("Best val acc", max(val_acc).cpu().numpy())
    print("Best val loss", min(val_loss))
    torch.save(model.state_dict(), "Model Checkpoints/" + model_name + ".pth")

    # Iterate over data.
    running_corrects = 0
    for inputs, labels in test_loader:
        inputs = inputs.to(device)
        labels = labels.to(device, dtype=torch.int64)

        optimizer.zero_grad()
        with torch.set_grad_enabled(False):
            outputs = model(inputs)
            _, preds = torch.max(outputs, 1)
        running_corrects += torch.sum(preds == labels.data)
        epoch_acc = running_corrects.double() / len(test_loader.dataset)
    print("Test acc:", epoch_acc.cpu().numpy())

