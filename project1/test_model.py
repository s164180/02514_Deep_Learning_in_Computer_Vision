from sklearn.metrics import classification_report
import torch
import numpy as np
import matplotlib.pyplot as plt
device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')


def test_classifier(model, test_loader, output_dict=False, digits=5):
    y_pred_lst = []
    y_test_lst = []
    for data, y_test in test_loader:
        data = data.to(device)
        model.eval()
        with torch.no_grad():
            output = model(data)
        y_pred = np.argmax(output.cpu().numpy(), axis=1)
        y_pred_lst.extend(y_pred)
        y_test_lst.extend(y_test.numpy())
    return classification_report(y_test_lst, y_pred_lst, output_dict=output_dict, digits=digits)


def show_classifier(model, test_loader, mis_class=False, false_pos=True):
    for data, y_test in test_loader:
        data = data.to(device)
        with torch.no_grad():
            output = model(data)

        y_pred = np.argmax(output.cpu().numpy(), axis=1)
        for i in range(len(y_test)):
            mask = (y_test[i] != y_pred[i]) and mis_class
            if y_pred[i] == false_pos and mask:
                print(y_pred[i])
                print(y_test[i])
                print(["Falsely", "Correctly"][y_test[i] == y_pred[i]], end=" ")
                print(f"predicted this was", end=" ")
                print(['a hotdog', 'not a hotdog'][y_pred[i]])
                plt.figure(figsize=(6, 6))
                plt.imshow(np.swapaxes(np.swapaxes(data[i].cpu().numpy(), 0, 2), 0, 1))
                plt.show()