

import numpy as np
from tqdm import tqdm
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
import pickle
import os
from PIL import Image
import glob


from torchvision import datasets, transforms

from utils import Hotdog_NotHotdog, train

from models import ResNetVGG, ResNet, ResNetBottle



if __name__ == '__main__':

    ## Test for GPU and set device
    if torch.cuda.is_available():
        print("The code will run on GPU.")
    else:
        print("The code will run on CPU. Go to Edit->Notebook Settings and choose GPU as the hardware accelerator")
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    ## Resize images and set datasets
    torch.manual_seed(0)
    size = 32
    N_val_images = 200


    # Apply data augmentation to trainset only
    train_transform = transforms.Compose([transforms.Resize((size, size)),
                                          transforms.RandomHorizontalFlip(),
                                          transforms.RandomVerticalFlip(),
                                          transforms.RandomRotation(45),
                                          transforms.ToTensor()])
    test_transform = transforms.Compose([transforms.Resize((size, size)),
                                         transforms.ToTensor()])

    batch_size = 24
    train_data = Hotdog_NotHotdog(train=True, transform=train_transform)
    train_set, val_set = torch.utils.data.random_split(train_data, [train_data.__len__() - 200, 200])
    train_loader = DataLoader(train_set, batch_size=batch_size, shuffle=True, num_workers=12)
    val_loader = DataLoader(val_set, batch_size=batch_size, shuffle=True, num_workers=12)
    test_data = Hotdog_NotHotdog(train=False, transform=test_transform)
    test_loader = DataLoader(test_data, batch_size=batch_size, shuffle=False, num_workers=12)

    ### Test resnet

    # Setup model and optimizer
    model = ResNet(filters = 64, input_size = size,input_dim = 3,num_res_blocks = 2,weight = False, bottleneck = False)


    model.to(device)    
    optimizer = torch.optim.Adam(model.parameters(),lr=0.0005)
    loss_fun = nn.CrossEntropyLoss()

    # Setup write to Tensorboard and run experiments
    out_dict = train(model, train_loader,val_loader,loss_fun,optimizer,device,num_epochs=100)