from train_model import train_model, get_n_params
from models import vgg11, AlexNet, ResNetVGG, ResNet, ResNetBottle
from data import load_data

from test_model import test_classifier, show_classifier
import torch
import torch.nn as nn
import numpy as np
import itertools
from random import sample, choice
import json
import pickle


def get_best_params(model_class, param_dict, n_samples):
    loss = nn.CrossEntropyLoss()
    keys, values = zip(*param_dict.items())

    # Returns list of dicts with parameter combinations
    all_comb_params = [dict(zip(keys, v)) for v in itertools.product(*values)]
    # Sample without replacement (random search)
    param_sample = sample(all_comb_params, n_samples)

    best_val_acc = 0
    for params in param_sample:

        exclude_keys = ['lr']
        new_d = {k: params[k] for k in set(list(params.keys())) - set(exclude_keys)}
        model = model_class(**new_d)
        model.to(device)

        # Sample a learning rate for Adam optimizer
        optimizer = torch.optim.Adam(model.parameters(), lr=params['lr'])
        # Sample a batch size
        batch_size = choice(np.arange(10, 66).astype(int))
        train_loader, val_loader, _ = load_data(batch_size, params['input_size'])
        dataloaders_dict = {'train': train_loader, 'val': val_loader}
        model, val_loss, val_acc = train_model(model, dataloaders_dict, loss, optimizer, num_epochs=50, patience=5)
        val_acc = max(val_acc)
        if val_acc > best_val_acc:
            batch_size_ = batch_size
            best_val_acc = val_acc
            best_model = model
            best_param = params

    return best_param, best_model, best_val_acc, batch_size_

def main():
    n_samples = 20

    vgg_params = {'input_size': [64, 128, 164, 224, 300],
                  'filters':  np.arange(16, 66, 2),
                  'lr': np.logspace(-4, -2, 20)
    }
    alex_params = {
                'input_size': [227],
                'filters': [18, 36, 72, 144],
                'lr': np.logspace(-4, -1, 6)
    }
    resnet_params = {
        'input_size': [64, 128, 224],
        'filters':  [16,32,64,128],
        'num_res_blocks': [1, 2, 3, 4],
        'lr': np.logspace(-4, -1, 6)
    }
    ResNetVGG_params = {
        'input_size': [64, 128, 224],
        'filters': [16,32,64,128],
        'num_res_blocks': [1, 2, 3, 4],
        'lr': np.logspace(-4, -1, 6)
    }

    resnetBottle_params = {
        'input_size': [64, 128, 224],
        'filters':  [16,32,64,128],
        'num_res_blocks': [1, 2, 3, 4],
        'lr': np.logspace(-4, -1, 6)
    }

    model_names = ["vgg11"]
    model_params = [vgg_params]
    models = [vgg11]

    for model, model_name, params in zip(models, model_names, model_params):
        best_param, best_model, best_val_acc, batch_size = get_best_params(model, params, n_samples)
        torch.save(best_model.state_dict(), "Model Checkpoints/" + model_name + ".pth")
        _, _, test_loader = load_data(batch_size, best_param['input_size'])
        test_res = test_classifier(best_model, test_loader, output_dict=True)

        with open("Results/TestResults_" + model_name, 'wb') as f:
            pickle.dump(test_res, f)
        with open("Results/BestParams_" + model_name, 'wb') as f:
            pickle.dump(best_param, f)

if __name__ == '__main__':
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
    main()