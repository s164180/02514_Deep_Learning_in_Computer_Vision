import torch.nn as nn


class vgg11(nn.Module):
    def __init__(self, filters, input_size):
        super(vgg11, self).__init__()
        self.convolutional = nn.Sequential(
            nn.Conv2d(3, filters, 3, padding=1),
            nn.BatchNorm2d(filters),
            nn.ReLU(inplace=True),

            nn.MaxPool2d(2),

            nn.Conv2d(filters, 2* filters, 3, padding=1),
            nn.BatchNorm2d(2 * filters),
            nn.ReLU(inplace=True),

            nn.MaxPool2d(2),

            nn.Conv2d(2 * filters, 4 * filters, 3, padding=1),
            nn.BatchNorm2d(4 * filters),
            nn.ReLU(inplace=True),

            nn.Conv2d(4 * filters, 4 * filters, 3, padding=1),
            nn.BatchNorm2d(4 * filters),
            nn.ReLU(inplace=True),

            nn.MaxPool2d(2),

            nn.Conv2d(4 * filters, 8 * filters, 3, padding=1),
            nn.BatchNorm2d(8 * filters),
            nn.ReLU(inplace=True),

            nn.Conv2d(8 * filters, 8 * filters, 3, padding=1),
            nn.BatchNorm2d(8 * filters),
            nn.ReLU(inplace=True),

            nn.MaxPool2d(2),

            nn.Conv2d(8 * filters, 8 * filters, 3, padding=1),
            nn.BatchNorm2d(8 * filters),
            nn.ReLU(inplace=True),

            nn.Conv2d(8 * filters, 8 * filters, 3, padding=1),
            nn.BatchNorm2d(8 * filters),
            nn.ReLU(inplace=True),

            nn.MaxPool2d(2)
        )
        spatial_dim = int(input_size / 2 ** 5)

        self.fully_connected = nn.Sequential(
            nn.Linear(8 * filters * spatial_dim * spatial_dim, 64 * filters),
            nn.ReLU(),
            nn.Dropout(),

            nn.Linear(64 * filters, 64 * filters),
            nn.ReLU(),
            nn.Dropout(),

            nn.Linear(64 * filters, 2),
            nn.LogSoftmax(dim=1))

    def forward(self, x):
        x = self.convolutional(x)
        x = x.view(x.size(0), -1)
        x = self.fully_connected(x)
        return x

        
class AlexNet(nn.Module):

    def __init__(self,filters,input_size):
        super(AlexNet,self).__init__()
        self.features = nn.Sequential(

        # Convolutional layer 1
        nn.Conv2d(3, filters, kernel_size=(11,11),stride=(4,4), padding=(2,2)),
        nn.BatchNorm2d(filters),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=3, stride=2, padding=0),

        # Convolutional layer 2
        nn.Conv2d(filters, int(filters*(8/3)), kernel_size=(5,5),padding=(2,2), stride=(1,1)),
        nn.BatchNorm2d(int(filters*(8/3))),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=3, stride=2, padding=0),

        # Convolutional layer 3
        nn.Conv2d(int(filters*(8/3)), filters*4, kernel_size=(3,3), stride=(1,1), padding=(1,1)),
        nn.BatchNorm2d(filters*4),
        nn.ReLU(inplace=True),

        # Convolutional layer 4
        nn.Conv2d(filters*4, filters*4, kernel_size=(3,3), stride=(1,1),padding=(1,1)),
        nn.BatchNorm2d(filters*4),
        nn.ReLU(inplace=True),

        # Convolutional layer 5
        nn.Conv2d(filters*4, int(filters*(8/3)), kernel_size=3, padding=1),
        nn.BatchNorm2d(int(filters*(8/3))),
        nn.ReLU(inplace=True),
        nn.MaxPool2d(kernel_size=3,stride=2, padding = 0))

        # Makes maxpooling 3 times
        spatial_dim = int((input_size - 11)/4) + 1
        spatial_dim = int((spatial_dim - 3)/2) + 1
        spatial_dim = int((spatial_dim - 3)/2) + 1
        spatial_dim = int((spatial_dim - 3)/2) + 1

        self.fully_connected = nn.Sequential(
            # Fully connected layer 1
            nn.Linear((int(filters*(8/3)))*spatial_dim*spatial_dim,4096),
            nn.ReLU(inplace=True),
            nn.Dropout(),

            # Fully connected layer 2
            nn.Linear(4096,4096),
            nn.ReLU(inplace=True),
            nn.Dropout(),

            # Fully connected layer 3
            nn.Linear(4096,2),
            # Return to softmax layer
            nn.Softmax(dim=1)
        )

    def forward(self,x):
        x = self.features(x)

        # Reshape
        x = x.view(x.size(0),-1)
        x = self.fully_connected(x)
        return x

class ResNetBlock(nn.Module):
    '''
    Resnet model with both projection (weight) and bottleneck included
    '''
    def __init__(self, n_in, n_out, weight = False,bottleneck = False):
        super(ResNetBlock, self).__init__()

        self.weight = weight
        self.bottleneck = bottleneck

        # Check if it's Bottleneck network
        if self.bottleneck:
            self.convolutional = nn.Sequential(
                nn.Conv2d(n_in, n_in  // 4, 1),
                nn.ReLU(),
                nn.Conv2d(n_in // 4, n_out // 4, 3, padding = 1),
                nn.ReLU(),
                nn.Conv2d(n_out // 4, n_out,1)
            )
        else:
            self.convolutional = nn.Sequential(
                nn.Conv2d(n_in, n_out, 3,padding=1),
                nn.ReLU(),
                nn.Conv2d(n_out, n_out, 3,padding=1)
            )

        # ''Linear layer'' <- Do it this way with conv2d since there is no need to resize and easier to work with
        if self.weight:
            self.linear = nn.Conv2d(n_in, n_out,1)
    def forward(self, x):
        out = None

        #Check if projection is required
        if self.weight:
            out = self.convolutional(x) + self.linear(x)
        else:
            out = self.convolutional(x) + x
        return out


class ResNetVGG(nn.Module): # Resnet that emulates the VGG topology
    def __init__(self, filters, input_size,input_dim = 3,num_res_blocks=3, weight=True, bottleneck=False):
        super(ResNetVGG, self).__init__()
        # First conv layers needs to output the desired number of features.
        conv_layers = [nn.Conv2d(input_dim, filters, kernel_size = 3, padding = 1),
                        nn.BatchNorm2d(filters), nn.ReLU(inplace=True), nn.MaxPool2d(2)]

        for i in range(num_res_blocks):
            conv_layers.append(ResNetBlock(n_in = filters * (2 ** i) , n_out = filters * 2**(i+1), weight = weight, bottleneck = bottleneck))
            conv_layers.append(nn.BatchNorm2d(2**(i+1) * filters))
            conv_layers.append(nn.MaxPool2d(2))

        self.res_blocks = nn.Sequential(*conv_layers)

        reduced_size = input_size // (2**(num_res_blocks + 1))

        self.dropout = nn.Dropout2d(p=0.5)

        self.fc = nn.Sequential(nn.Linear(reduced_size * reduced_size * filters * 2**(num_res_blocks), 64 * filters),
                                nn.ReLU(),
                                nn.Linear(64 * filters, 64 * filters),
                                nn.ReLU(),
                                nn.Linear(64 * filters, 2),
                                nn.LogSoftmax(dim=1))

    def forward(self, x):
        out = self.res_blocks(x)
        # reshape x so it becomes flat, except for the first dimension (which is the minibatch)
        out = self.dropout(out)
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        return out


class ResNet(nn.Module): # Resnet that emulates the VGG topology
    def __init__(self, filters, input_size,input_dim = 3,num_res_blocks=3, weight=False, bottleneck=False):
        super(ResNet, self).__init__()
        # First conv layers needs to output the desired number of features.
        conv_layers = [nn.Conv2d(input_dim, filters, kernel_size = 3, padding = 1),
                        nn.BatchNorm2d(filters), nn.ReLU(inplace=True), nn.MaxPool2d(2)]

        for i in range(num_res_blocks):
            conv_layers.append(ResNetBlock(n_in = filters * (2 ** i) , n_out = filters * 2**(i), weight = weight, bottleneck = bottleneck))
            conv_layers.append(nn.BatchNorm2d(2**(i) * filters))
            conv_layers.append(nn.MaxPool2d(2))
            conv_layers.append(nn.Conv2d(filters * (2 ** i) ,filters * (2 ** (i+1)), 3, padding=1))

        self.res_blocks = nn.Sequential(*conv_layers)

        reduced_size = input_size // (2**(num_res_blocks + 1))

        self.dropout = nn.Dropout2d(p=0.5)

        self.fc = nn.Sequential(nn.Linear(reduced_size * reduced_size * filters * 2**(num_res_blocks), 64 * filters),
                                nn.ReLU(),
                                nn.Linear(64 * filters, 64 * filters),
                                nn.ReLU(),
                                nn.Linear(64 * filters, 2),
                                nn.LogSoftmax(dim=1))

    def forward(self, x):
        out = self.res_blocks(x)
        # reshape x so it becomes flat, except for the first dimension (which is the minibatch)
        out = self.dropout(out)
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        return out

class ResNetBottle(nn.Module): # Resnet that emulates the VGG topology
    def __init__(self, filters, input_size,input_dim = 3,num_res_blocks=3, weight = False, bottleneck = True):
        super(ResNetBottle, self).__init__()
        # First conv layers needs to output the desired number of features.
        conv_layers = [nn.Conv2d(input_dim, filters, kernel_size = 3, padding = 1),
                        nn.BatchNorm2d(filters), nn.ReLU(inplace=True), nn.MaxPool2d(2)]

        for i in range(num_res_blocks):
            conv_layers.append(ResNetBlock(n_in = filters * (2 ** i) , n_out = filters * 2**(i), weight = weight, bottleneck = bottleneck))
            conv_layers.append(nn.BatchNorm2d(2**(i) * filters))
            conv_layers.append(nn.MaxPool2d(2))
            conv_layers.append(nn.Conv2d(filters * (2 ** i) ,filters * (2 ** (i+1)), 3, padding=1))

        self.res_blocks = nn.Sequential(*conv_layers)

        reduced_size = input_size // (2**(num_res_blocks + 1))

        self.dropout = nn.Dropout2d(p=0.5)

        self.fc = nn.Sequential(nn.Linear(reduced_size * reduced_size * filters * 2**(num_res_blocks), 64 * filters),
                                nn.ReLU(),
                                nn.Linear(64 * filters, 64 * filters),
                                nn.ReLU(),
                                nn.Linear(64 * filters, 2),
                                nn.LogSoftmax(dim=1))

    def forward(self, x):
        out = self.res_blocks(x)
        # reshape x so it becomes flat, except for the first dimension (which is the minibatch)
        out = self.dropout(out)
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        return out
