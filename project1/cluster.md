
## Guide til brug af cluster
Man kan bruge clusteren på to måder  - interaktivt (brug denne for at teste eller debugge) eller vha. job scripts
(brug denne til store jobs). Brug filezilla eller rsync til at overføre filer til clusteren

## Interaktiv brug af clusteren
* Det er vigtigt, at i logger ind på s*studienummer*@login2.gbar.dtu.dk
* Kør følgende i terminalen:
  * module load python3 # loader python
  * sxm2sh # skifter til gpu node
  * python3 -i script.py # kører node intearkivt
 

## Brug af cluster vha. script 
* Brug filen "gpu.sh" som en skabelon til opsætning
* Kør bsub < gpu.sh

## Litteratur (kommandoer osv.)
https://www.hpc.dtu.dk/?page_id=1416

