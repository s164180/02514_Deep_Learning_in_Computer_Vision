# Todo
* Input normalization
* Inceptionism
* Adversarial examples
* Transfer learning
* Consider if validation set should be rotated, flipped, etc


## Qahir-TODO
 * Implement Squeeze and excite 
 



# Links
* Enlarging smaller images before inputting into convolutional neural network: zero-padding vs. interpolation  
 * Link: https://journalofbigdata.springeropen.com/articles/10.1186/s40537-019-0263-7#Sec3
* ResNet: https://arxiv.org/abs/1512.03385
* Squeeze-and-excite: https://arxiv.org/abs/1709.01507
