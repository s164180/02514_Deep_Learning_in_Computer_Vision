import matplotlib.pyplot as plt
import matplotlib.patches as patches
import numpy as np


class boundBox(object):
    def __init__(self, j, i, dim, input_size, class_probs):
        self.i = i
        self.j = j
        self.input_size = input_size
        self.dim = dim
        self.grid_length = input_size / dim
        self.class_probs = class_probs
        self.top_left_corner = [i*self.grid_length, j*self.grid_length]
        self.bottom_right_corner = [(i+1)*self.grid_length, (j+1)*self.grid_length+1]
        self.top_right_corner = [(i+1)*self.grid_length, (j+1)*self.grid_length]
        self.bottom_left_corner = [(i+1) * self.grid_length, (j+1)*self.grid_length]
        self.area = self.grid_length**2

    def draw_BB(self, image):
        im = np.array(image)
        fig, ax = plt.subplots(1)
        ax.imshow(im)
        rect = patches.Rectangle((self.top_left_corner[0], self.top_left_corner[1]),
                                 self.grid_length, self.grid_length, linewidth=1,
                                 edgecolor='r', facecolor='none')
        ax.add_patch(rect)
        plt.show()

    def get_overlap_area(self, other_bb):
        lenx = min(self.bottom_right_corner[1], other_bb.bottom_right_corner[1]) - \
               max(self.bottom_left_corner[1], other_bb.bottom_right_corner[1])
        leny = min(self.bottom_right_corner[0], other_bb.bottom_right_corner[0]) - \
               max(self.bottom_left_corner[0], other_bb.bottom_right_corner[0])
        return lenx*leny

    def get_total_area(self, other_bb):
        return self.area + other_bb.area - self.get_overlap_area(other_bb)
