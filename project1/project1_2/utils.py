import numpy as np
from tqdm import tqdm
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
import pickle
import os
from PIL import Image
import glob
import scipy.io
from torchvision import datasets, transforms
import matplotlib.pyplot as plt

class SVHNTrain(torch.utils.data.Dataset):
    def __init__(self, train, transform = None):
        'Initialzation'

        self.data_path = './data/train_32x32.mat' if train else './data/test_32x32.mat'
        self.data = scipy.io.loadmat(self.data_path)
        self.X = self.data['X']
        self.y = self.data['y']

        self.max_data = 73257 if train else 26032 # These are the respective lengths in the mat files of test and train
        self.max_image = 33402 if train else 13068 # Thse are the respective number of images in the SVHN folder
        self.transform = transform


        self.image_paths = './SVHN/train/' if train else  './SVHN/test/'

    def __len__(self):
        'Returns the total number of samples'
        return self.max_data + self.max_image

    def __getitem__(self, idx):
        'Generates one sample of data'

        # Generate random cropped image
        if idx < self.max_image:
            image_path = self.image_paths + f'{idx + 1}.png'# Get path to image
            image = Image.open(image_path)

            X = transforms.RandomCrop(32,pad_if_needed=True)(image)
            X = transforms.ToTensor()(X).byte()
            y = np.array([10], dtype='uint8') # Means no digit
        else: # We pick an index corresponding to a digit
            ind = idx - self.max_image - 1
            X = self.X[:,:,:,ind]
            y = self.y[ind]
        #X = self.transform(X)
        return X, y


### Generate class of no-digits

if __name__ == '__main__':

    ### This code runs a quick test of the dataset to see it works as expected

    ## Test for GPU and set device
    if torch.cuda.is_available():
        print("The code will run on GPU.")
    else:
        print("The code will run on CPU. Go to Edit->Notebook Settings and choose GPU as the hardware accelerator")
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    torch.manual_seed(0)
    size = 12
    N_val_images = 20000

    # Apply data augmentation to trainset only
    train_transform = None

    batch_size = 12
    train_data = SVHNTrain(train=True, transform=train_transform)

    ## Test if they work
    train_data.__getitem__(33402)
    train_data.__getitem__(33402 + 73257) # Cropped images of SVHN

    ## Leets see som images of digits
    batch_size = 64
    train_loader = DataLoader(train_data, batch_size=batch_size, shuffle=True, num_workers=1)
    images, labels = next(iter(train_loader))

    ## Lets see some randomly cropped images
    plt.figure(figsize=(20, 10))

    for i in range(21):
        plt.subplot(5, 7, i + 1)
        plt.imshow(images[i].numpy()[0], 'gray')
        plt.title(labels[i].item())
        plt.axis('off')


