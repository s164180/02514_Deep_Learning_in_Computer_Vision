
import torch
import torch.nn as nn
import torch.nn.functional as F


class ResNetBlock(nn.Module):
    '''
    Resnet model with both projection (weight) and bottleneck included
    '''
    def __init__(self, n_features, weight = False,bottleneck = False):
        super(ResNetBlock, self).__init__()

        self.weight = weight
        self.bottleneck = bottleneck

        # Check if it's Bottleneck network
        if self.bottleneck:
            self.convolutional = nn.Sequential(
                nn.Conv2d(n_features, n_features  // 4, 1),
                nn.ReLU(),
                nn.Conv2d(n_features // 4, n_features // 4, 3, padding = 1),
                nn.ReLU(),
                nn.Conv2d(n_features // 4, n_features,1)
            )
        else:
            self.convolutional = nn.Sequential(
                nn.Conv2d(n_features, n_features, 3,padding=1),
                nn.ReLU(),
                nn.Conv2d(n_features, n_features, 3,padding=1)
            )

        # ''Linear layer'' <- Do it this way with conv2d since there is no need to resize and easier to work with
        if self.weight:
            self.linear = nn.Conv2d(n_features, n_features,1)
    def forward(self, x):
        out = None

        #Check if projection is required
        if self.weight:
            out = self.convolutional(x) + self.linear(x)
        else:
            out = self.convolutional(x) + x
        return out

class ResNet(nn.Module):
    def __init__(self, filters, input_size,input_dim = 3,num_res_blocks=3,weight = False, bottleneck = False):
        super(ResNet, self).__init__()
        # First conv layers needs to output the desired number of features.
        conv_layers = [nn.Conv2d(input_dim, filters, kernel_size = 3, padding = 1),
                        nn.BatchNorm2d(filters), nn.ReLU(inplace=True), nn.MaxPool2d(2)]

        for i in range(num_res_blocks):
            conv_layers.append(ResNetBlock(n_features = filters * (2 ** i) , weight = weight, bottleneck = bottleneck))
            conv_layers.append(nn.BatchNorm2d(2**(i) * filters))
            conv_layers.append(nn.MaxPool2d(2))

        self.res_blocks = nn.Sequential(*conv_layers)

        reduced_size = input_size // (2**(num_res_blocks + 1))

        self.dropout = nn.Dropout2d(p=0.5)

        self.fc = nn.Sequential(nn.Linear(reduced_size * reduced_size * filters * 2**(num_res_blocks), 64 * filters),
                                nn.ReLU(),
                                nn.Linear(64 * filters, 64 * filters),
                                nn.ReLU(),
                                nn.Linear(64 * filters, 2),
                                nn.LogSoftmax(dim=1))

    def forward(self, x):
        out = self.res_blocks(x)
        # reshape x so it becomes flat, except for the first dimension (which is the minibatch)
        out = self.dropout(out)
        out = out.view(out.size(0), -1)
        out = self.fc(out)
        return out