

##TODO Make dataloader
##TODO read papers
##TODO implement YOLO


## Crop images and get class that is no-digit

import numpy as np
from tqdm import tqdm
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.utils.data import DataLoader
import pickle
import os
from PIL import Image
import glob


from torchvision import datasets, transforms


batch_size = 64

train_svhn = datasets.SVHN('./data', split="train", download=True, transform=transforms.ToTensor())
train_cifar = datasets.CIFAR10('./data', train=True, download=True, transform=transforms.ToTensor())
train_cifar.targets = np.asarray(train_cifar.targets)
train_cifar.targets[:] = 10
trainset = torch.utils.data.ConcatDataset((train_cifar, train_svhn))


test_svhn = datasets.SVHN('./data', split="test", download=True, transform=transforms.ToTensor())
test_cifar = datasets.CIFAR10('./data', train=False, download=True, transform=transforms.ToTensor())
test_cifar.targets = np.asarray(test_cifar.targets)
test_cifar.targets[:] = 10
testset = torch.utils.data.ConcatDataset((test_cifar, test_svhn))

train_loader = DataLoader(trainset, batch_size=batch_size, shuffle=True, num_workers=1)
test_loader = DataLoader(testset, batch_size=batch_size, shuffle=False, num_workers=1)